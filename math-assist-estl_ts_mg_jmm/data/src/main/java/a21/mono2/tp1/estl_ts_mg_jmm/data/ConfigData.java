package a21.mono2.tp1.estl_ts_mg_jmm.data;

import a21.mono2.tp1.estl_ts_mg_jmm.view.inversion.IConfigDataView;
import a21.mono2.tp1.estl_ts_mg_jmm.view.inversion.IEquationDataView;

public class ConfigData implements IConfigDataView {

    private int id;
    private IEquationDataView equation;


    public ConfigData(){}
    public ConfigData(IEquationDataView equation) {
        this.equation = equation;
    }

    public ConfigData(int id, IEquationDataView equation) {
        this.id = id;
        this.equation = equation;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public IEquationDataView getEquation() {
        return equation;
    }

    @Override
    public void setEquation(IEquationDataView equation) {
        this.equation = equation;
    }

    @Override
    public String toString(){
        return getEquation().getFunction();
    }
}
