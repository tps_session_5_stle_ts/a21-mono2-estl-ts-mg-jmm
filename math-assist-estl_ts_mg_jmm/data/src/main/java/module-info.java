module data {
    requires view;
    requires model;

    exports a21.mono2.tp1.estl_ts_mg_jmm.data;
    opens a21.mono2.tp1.estl_ts_mg_jmm.data;
}