module view {
    requires javafx.fxml;
    requires javafx.controls;

    exports a21.mono2.tp1.estl_ts_mg_jmm.view.inversion;
    exports a21.mono2.tp1.estl_ts_mg_jmm.view.factory;
    exports a21.mono2.tp1.estl_ts_mg_jmm.view.exception;
    exports a21.mono2.tp1.estl_ts_mg_jmm.view;

    opens fxml;
    opens a21.mono2.tp1.estl_ts_mg_jmm.view to javafx.controls, javafx.fxml, javafx.graphics;
}