package a21.mono2.tp1.estl_ts_mg_jmm.view.factory;

import a21.mono2.tp1.estl_ts_mg_jmm.view.inversion.IControllerFactoryView;

/**
 * The class responsible to contain the Controller's ControllerFactory.
 */
public class ViewControllerFactory {
    private static IControllerFactoryView factory;

    public static IControllerFactoryView getFactoryInstance() {
        return factory;
    }

    public static void setFactory(IControllerFactoryView factory) {
        ViewControllerFactory.factory = factory;
    }
}
