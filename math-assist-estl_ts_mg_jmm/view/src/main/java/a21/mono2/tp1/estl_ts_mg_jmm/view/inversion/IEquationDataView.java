package a21.mono2.tp1.estl_ts_mg_jmm.view.inversion;

public interface IEquationDataView {

    int getId();
    String getCategory();
    String getParameter();
    String getFunction();
    String getExpression();
    void setId(int id);
    void setCategory(String category);
    void setParameter(String parameter);
    void setFunction(String function);
    void setExpression(String expression);
    String toString();
    String toStringSearch();
}
