package a21.mono2.tp1.estl_ts_mg_jmm.view.inversion;
import a21.mono2.tp1.estl_ts_mg_jmm.view.exception.ViewInvalidEquationException;

import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

public interface IEquationControllerView {
    IEquationDataView createEquationData();

    IEquationDataView createEquationData(String category, String function);

    IEquationDataView setFullEquationDataView(IEquationDataView equationData);

    IEquationDataView createEquationData(int id,String category, String function);

    boolean isEquationValid(String function);

    List<IEquationDataView> getDatabaseEquations();

    void modifyDatabaseEquation(IEquationDataView equationData);

    IEquationDataView addDatabaseEquation(String category, String equation) throws ViewInvalidEquationException;

    void deleteDatabaseEquation(int equationId);

    Runnable getRechercheSimple(String entree);

    Runnable getRechercheComplexe(String entree);

    LinkedBlockingQueue<String> getFilteredValues();
}
