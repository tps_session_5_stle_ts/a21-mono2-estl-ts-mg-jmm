package a21.mono2.tp1.estl_ts_mg_jmm.view;

import a21.mono2.tp1.estl_ts_mg_jmm.view.inversion.IEquationDataView;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class EquationDecorator implements IEquationDataView {
    private IEquationDataView innerEquation;

    private SimpleIntegerProperty idProperty;
    private SimpleStringProperty categoryProperty;
    private SimpleStringProperty functionProperty;
    private SimpleStringProperty parameterProperty;
    private SimpleStringProperty expressionProperty;

    public EquationDecorator(IEquationDataView equation) {
        this.innerEquation = equation;
        idProperty = new SimpleIntegerProperty(equation.getId());
        categoryProperty = new SimpleStringProperty(equation.getCategory());
        functionProperty = new SimpleStringProperty(equation.getFunction());
        parameterProperty = new SimpleStringProperty(equation.getParameter());
        expressionProperty = new SimpleStringProperty(equation.getExpression());
    }

    public SimpleIntegerProperty idProperty() {
        return idProperty;
    }

    public SimpleStringProperty categoryProperty() {
        return categoryProperty;
    }

    public SimpleStringProperty functionProperty() {
        return functionProperty;
    }

    public SimpleStringProperty parameterProperty() {
        return parameterProperty;
    }

    public SimpleStringProperty expressionProperty() {
        return expressionProperty;
    }

    public IEquationDataView getInnerEquation() {
        return innerEquation;
    }

    @Override
    public int getId() {
        return idProperty.get();
    }

    @Override
    public String getCategory() {
        return categoryProperty.get();
    }

    @Override
    public String getParameter() {
        return parameterProperty.get();
    }

    @Override
    public String getFunction() {
        return functionProperty.get();
    }

    @Override
    public String getExpression() {
        return expressionProperty.get();
    }

    @Override
    public void setId(int id) {
        innerEquation.setId(id);
        idProperty.set(id);
    }

    @Override
    public void setCategory(String category) {
        innerEquation.setCategory(category);
        categoryProperty.set(category);
    }

    @Override
    public void setParameter(String parameter) {
        innerEquation.setParameter(parameter);
        parameterProperty.set(parameter);
    }

    @Override
    public void setFunction(String function) {
        innerEquation.setFunction(function);
        functionProperty.set(function);
    }

    @Override
    public void setExpression(String expression) {
        innerEquation.setExpression(expression);
        expressionProperty.set(expression);
    }

    @Override
    public String toStringSearch() {
        return innerEquation.toStringSearch();
    }
}
