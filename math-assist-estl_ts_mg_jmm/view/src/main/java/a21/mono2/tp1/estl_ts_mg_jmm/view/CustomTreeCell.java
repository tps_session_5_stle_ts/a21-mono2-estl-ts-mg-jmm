package a21.mono2.tp1.estl_ts_mg_jmm.view;

import a21.mono2.tp1.estl_ts_mg_jmm.view.exception.ViewInvalidEquationException;
import a21.mono2.tp1.estl_ts_mg_jmm.view.inversion.IEquationControllerView;
import a21.mono2.tp1.estl_ts_mg_jmm.view.inversion.IEquationDataView;
import a21.mono2.tp1.estl_ts_mg_jmm.view.treeCellItems.CategorieFolder;
import a21.mono2.tp1.estl_ts_mg_jmm.view.utilTreeView.UtilTreeView;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TreeItem;
import javafx.scene.control.cell.TextFieldTreeCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.StringConverter;

import java.io.InputStream;

public class CustomTreeCell extends TextFieldTreeCell<IEquationDataView> {

    private ContextMenu equationMenu;
    private ContextMenu categorieMenu;
    private ContextMenu rootMenu;
    private IEquationControllerView controller;
    private UtilTreeView utilTreeView;


    public CustomTreeCell(IEquationControllerView controller, UtilTreeView treeView){
        super();
        this.controller = controller;
        this.utilTreeView = new UtilTreeView(this.controller);
        equationMenu = new ContextMenu();
        categorieMenu = new ContextMenu();
        rootMenu = new ContextMenu();


        setConverter(new Converter());
        MenuItem addEquation = new MenuItem("Add Equation");
        MenuItem addCategory = new MenuItem("Add Category");
        MenuItem addCategoryToRoot = new MenuItem("Add Category");
        MenuItem deleteCategory = new MenuItem("Delete Category");

        MenuItem deleteEquation = new MenuItem("delete Equation");

        addEquation.setOnAction((n)->{

            String cat = CustomTreeCell.this.getTreeItem().getValue().getCategory();
            IEquationDataView equation = null;
            try {
                equation = controller.addDatabaseEquation(cat,"f(x) = x + 2");
                CustomTreeCell.this.getTreeItem().getChildren().add(new TreeItem<>(equation));
            } catch (ViewInvalidEquationException e) {
                e.printStackTrace();
            }
        });
        addCategoryToRoot.setOnAction(event -> {
            String category = getTreeItem().getValue().getCategory();
            TreeItem<IEquationDataView> treeItem;
            treeItem = new TreeItem<>(new CategorieFolder("category"));
            try {
                treeItem.getChildren().add(new TreeItem<>(
                        controller.addDatabaseEquation(treeItem.getValue().getCategory(),
                                "f(x) = x + 2")));
                getTreeItem().getChildren().add(treeItem);
            } catch (ViewInvalidEquationException e) {
                e.printStackTrace();
            }
        });
        addCategory.setOnAction((n)->{
            String category = getTreeItem().getValue().getCategory();
            TreeItem<IEquationDataView> treeItem;
                treeItem = new TreeItem<>(new CategorieFolder(category+"/"+"category"));
                try {
                    treeItem.getChildren().add(new TreeItem<>(
                            controller.addDatabaseEquation(treeItem.getValue().getCategory(),
                                    "f(x) = x")));
                    getTreeItem().getChildren().add(treeItem);
                } catch (ViewInvalidEquationException e) {
                    e.printStackTrace();
                }
        });
        deleteCategory.setOnAction((n) ->{
            utilTreeView.deleteChildren(getTreeItem().getChildren().get(0));
            getTreeItem().getParent().getChildren().remove(getTreeItem());
        });

        deleteEquation.setOnAction((n)->{
            
            controller.deleteDatabaseEquation(getTreeItem().getValue().getId());
            getTreeItem().getParent().getChildren().remove(getTreeItem());
        });

        equationMenu.getItems().add(deleteEquation);
        categorieMenu.getItems().addAll(addEquation,addCategory,deleteCategory);
        rootMenu.getItems().add(addCategoryToRoot);
    }


    @Override
    public void updateItem(IEquationDataView equationData, boolean b) {
        super.updateItem(equationData, b);
        InputStream in = this.getClass().getResourceAsStream("images/dossier.png");
        ImageView image = new ImageView(new Image(in,15,15,true,true));
        if (!b){
            if (getTreeItem().isLeaf() && !(getTreeItem().getValue() instanceof CategorieFolder)){
                setGraphic(null);
                setText(equationData.getFunction());
                setContextMenu(equationMenu);
            } else{
                setGraphic(image);
                String[] categories = equationData.getCategory().split("/");
                String text = categories[categories.length-1] + " ("
                        + utilTreeView.nombreParCategorie(getTreeItem(),getTreeItem().getValue().getCategory())+")";
                setText(text);
                if (!(getTreeItem().getValue().getCategory().equalsIgnoreCase("root"))){
                    setContextMenu(categorieMenu);
                }else {
                    setContextMenu(rootMenu);
                }

            }
        }
    }



    public class Converter extends StringConverter<IEquationDataView> {

        @Override
        public String toString(IEquationDataView equation) {
            return equation.getFunction();
        }

        @Override
        public IEquationDataView fromString(String s) {

            IEquationDataView retEquation = null;
            if (controller.isEquationValid(s) && !(getTreeItem().getValue() instanceof CategorieFolder)) {
                int id = getTreeItem().getValue().getId();
                String categorie = getTreeItem().getValue().getCategory();
                retEquation = controller.createEquationData(id,categorie,s);
                controller.modifyDatabaseEquation(retEquation);
                System.out.println("id: " + retEquation.getId());
                return retEquation;
            } else if(getTreeItem().getValue() instanceof CategorieFolder){
                String[] path = getTreeItem().getValue().getCategory().split("/");
                if (s.isEmpty()||s.isBlank()){
                    return new CategorieFolder(getTreeItem().getValue().getCategory());
                } else {
                    path[path.length-1] = s;
                    StringBuilder sb = new StringBuilder();
                    sb.append(path[0]);
                    for (int i = 1; i < path.length; i++) {
                        sb.append("/").append(path[i]);
                    }
                    getTreeItem().getValue().setCategory(sb.toString());
                    utilTreeView.updateCategoryInChildren(getTreeItem().getChildren().get(0),s,1);
                    return new CategorieFolder(s);
                }

            } else {
                return getTreeItem().getValue();
            }

        }
    }
}
