package a21.mono2.tp1.estl_ts_mg_jmm.view;

import a21.mono2.tp1.estl_ts_mg_jmm.view.exception.ViewInvalidEquationException;
import a21.mono2.tp1.estl_ts_mg_jmm.view.factory.ViewControllerFactory;
import a21.mono2.tp1.estl_ts_mg_jmm.view.inversion.IEquationDataView;
import a21.mono2.tp1.estl_ts_mg_jmm.view.inversion.IEquationControllerView;
import a21.mono2.tp1.estl_ts_mg_jmm.view.treeCellItems.CategorieFolder;
import a21.mono2.tp1.estl_ts_mg_jmm.view.utilTreeView.UtilTreeView;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Side;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.*;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * The View responsible for the equation.fxml file.
 */
public class EquationView implements Initializable {

    @FXML
    private TableView<IEquationDataView> eq_TableView;

    @FXML
    private Tab eq_Tab_Tree;

    @FXML
    private Tab eq_Tab_Table;

    @FXML
    private TextField eq_Equation_Table;

    @FXML
    private TextField eq_Equation_Tree;

    @FXML
    private TreeView<IEquationDataView> eq_TreeView;

    private CustomContextMenu eq_autoComplete;

    private GraphiqueView graphiqueController;

    private IEquationControllerView controller;

    private IEquationDataView selectedEquationData;

    private UtilTreeView utilTreeView;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.controller = ViewControllerFactory.getFactoryInstance().createEquationController();
        this.utilTreeView = new UtilTreeView(controller);

        eq_Tab_Tree.setOnSelectionChanged(event -> refreshTreeView());
        eq_Tab_Table.setOnSelectionChanged(event -> {
            addDatabaseEquations();
        });

        // Add the equations in the database in the listView.
        addDatabaseEquations();

        // Initialize ContextMenu
        eq_autoComplete = new CustomContextMenu(393);
        eq_autoComplete.setMaxHeight(200);
        initializeAutoCompleteMenu(eq_Equation_Table);
        initializeAutoCompleteMenu(eq_Equation_Tree);

        // Add listeners.
        addListViewSelectedItemChangeListener();
        addTreeViewSelectedItemChangeListener();
        addEquationTextFieldKeyTypedListener();

        eq_TreeView.setCellFactory(fullEquationDataTreeView -> new CustomTreeCell(controller,utilTreeView));
    }

    /**
     * Add OnKeyTyped listener on eq_Equation.
     */
    private void addEquationTextFieldKeyTypedListener() {
        // When a key is typed.
        this.eq_Equation_Table.setOnKeyTyped(keyEvent -> {

            // Get a list of TableView's equations with the input text as Function.
            List<IEquationDataView> equationsWithInputFunction =
                    this.eq_TableView.getItems().stream()
                            .filter(x -> x.getFunction().equals(getEquationText()))
                            .collect(Collectors.toList());

            // If there is a FullEquationData in the list.
            if (equationsWithInputFunction.size() != 0) {
                // Select the FullEquationData with the Function.
                this.eq_TableView.getSelectionModel().select(equationsWithInputFunction.get(0));
                // Set the position caret to the end of the text.
                this.eq_Equation_Table.positionCaret(getEquationText().length());
                // Allow the user to delete the equation.
                //this.eq_Erase.setDisable(false);
            }
            // If there is no FullEquationData with the input Function.
            else {
                // Do not allow the user to delete the equation.
                //this.eq_Erase.setDisable(true);
            }

        });
    }

    /**
     * Add a ChangeListener on eq_TableView selected item (FullEquationData).
     */
    private void addListViewSelectedItemChangeListener() {
        this.eq_TableView.getSelectionModel().selectedItemProperty().addListener(((observableValue, equation, t1) -> {

            selectedEquationData = t1;
            if (selectedEquationData != null) {
                // Draw the graph corresponding to the Function.
                this.graphiqueController.drawNewGraphique(t1.getFunction());

                // To set the eq_Equation text to the new selection's Function.
                //this.eq_Equation_Table.setText(t1.getFunction());

                // Disable Add button.
                //this.eq_Add.setDisable(true);

            } else {
                // Clear graph elements.
                graphiqueController.clearGraphique();
                // Clear eq_Equation text.
                //eq_Equation_Table.clear();
            }
        }));
    }

    private void addTreeViewSelectedItemChangeListener() {
        this.eq_TreeView.getSelectionModel().selectedItemProperty().addListener(((observableValue, equation, t1) -> {

            if (t1 != null && !(t1.getValue() instanceof CategorieFolder)){
                selectedEquationData = t1.getValue();
                if (selectedEquationData != null) {
                    // Draw the graph corresponding to the Function.
                    this.graphiqueController.drawNewGraphique(t1.getValue().getFunction());

                    // To set the eq_Equation text to the new selection's Function.
                    //this.eq_Equation_Table.setText(t1.getFunction());

                    // Disable Add button.
                    //this.eq_Add.setDisable(true);

                } else {
                    // Clear graph elements.
                    graphiqueController.clearGraphique();
                    // Clear eq_Equation text.
                    //eq_Equation_Table.clear();
                }
            }
        }));
    }

    /**
     * Add the database's equations in eq_ListView.
     */
    private void addDatabaseEquations() {
        eq_TableView.setEditable(false);
        eq_TableView.getItems().clear();

        TableColumn<IEquationDataView, String> idColumn = (TableColumn<IEquationDataView, String>) eq_TableView.getColumns().get(0);
        TableColumn<IEquationDataView, String> categoryColumn = (TableColumn<IEquationDataView, String>) eq_TableView.getColumns().get(1);
        TableColumn<IEquationDataView, String> functionColumn = (TableColumn<IEquationDataView, String>) eq_TableView.getColumns().get(2);

        idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        categoryColumn.setCellValueFactory(new PropertyValueFactory<>("category"));
        functionColumn.setCellValueFactory(new PropertyValueFactory<>("function"));

        // Rend les colonnes category et function éditables
        categoryColumn.setCellFactory(c -> new CustomTableCell(controller, "category"));
        functionColumn.setCellFactory(c -> new CustomTableCell(controller, "function"));

        // Set le menu clique droit
        ContextMenu menu = new ContextMenu();
        MenuItem addItem = new MenuItem("Add equation");
        MenuItem deleteItem = new MenuItem("Delete equation");
        menu.getItems().addAll(addItem, deleteItem);
        eq_TableView.setContextMenu(menu);

        // Set l'action d'ajouter dans le menu clique droit
        addItem.setOnAction((a) -> {
            try {
                eq_TableView.getItems().add(new EquationDecorator(controller.addDatabaseEquation("Math","f(x) = x")));
                eq_TableView.getSelectionModel().selectLast();
                eq_TableView.scrollTo(eq_TableView.getItems().size() - 1);
                eq_TableView.refresh();
            } catch (ViewInvalidEquationException e) {
                e.printStackTrace();
            }
        });

        // Set l'action de supprimer dans le menu clique droit
        deleteItem.setOnAction((a) -> {
            controller.deleteDatabaseEquation(eq_TableView.getSelectionModel().getSelectedItem().getId());
            eq_TableView.getItems().remove(eq_TableView.getSelectionModel().getSelectedItem());
            eq_TableView.refresh();
        });

        // Rempli la TableView
        for (IEquationDataView equation: this.controller.getDatabaseEquations()) {
            eq_TableView.getItems().add(new EquationDecorator(equation));
        }

        eq_TableView.setEditable(true);
        eq_TableView.refresh();
    }

    /**
     * Initialise l'autocomplete
     * @param eq_Equation
     */
    private void initializeAutoCompleteMenu(TextField eq_Equation) {
        eq_Equation.addEventFilter(MouseEvent.MOUSE_PRESSED, e -> {
            if (e.getButton() == MouseButton.SECONDARY) {
                e.consume();
            }
        });

        eq_Equation.setContextMenu(eq_autoComplete);

        eq_Equation.textProperty().addListener((observableValue, s, entree) -> {
            List<MenuItem> menuItems = new ArrayList<>();
            eq_autoComplete.hide();
            eq_autoComplete = new CustomContextMenu(393);

            if (entree != null && !entree.equals("")) {
                ExecutorService pool = Executors.newFixedThreadPool(5);
                LinkedBlockingQueue<String> resultats = new LinkedBlockingQueue<>();

                // Effectue une recherche simple
                pool.submit(controller.getRechercheSimple(entree));

                // Effectue une recherche complexe
                //pool.submit(controller.getRechercheComplexe(entree));

                // Va chercher le contenu de FilteredValues dans le modèle et les met dans resultats
                pool.submit(() -> {
                    while(controller.getFilteredValues().size() > 0) {
                        try {
                            String resultat = controller.getFilteredValues().take();
                            resultats.put(resultat);
                            System.out.println("resultat: " + resultat);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });

                // Va chercher le contenue de resultats et en fait des MenuItem
                pool.submit(() -> {
                    while(resultats.size() > 0) {
                        try {
                            String resultat = resultats.take();
                            Label itemLabel = new Label(resultat);
                            itemLabel.setPrefWidth(eq_Equation.getWidth() - 22);

                            MenuItem menuItem = new MenuItem();
                            menuItem.setGraphic(itemLabel);
                            setMenuItemActions(eq_Equation, menuItem);

                            menuItems.add(menuItem);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

                    // Update le contenu du menu eq_autoComplete
                    Platform.runLater(() -> {
                        if (menuItems.size() > 0) {
                            eq_autoComplete.getItems().addAll(menuItems.get(0));
                            eq_autoComplete.show(eq_Equation, Side.BOTTOM, 0, 0);
                            if (menuItems.size() > 1) {
                                eq_autoComplete.getItems().addAll(menuItems.stream().skip(1).collect(Collectors.toList()));
                            }
                        }
                    });
                });

                try {
                    pool.shutdown();
                    pool.awaitTermination(60, TimeUnit.SECONDS);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                eq_autoComplete.hide();
            }
        });
    }

    /**
     * Set les listeneners des MenuItem de l'autocomplete
     * @param eq_Equation
     * @param menuItem
     */
    private void setMenuItemActions(TextField eq_Equation, MenuItem menuItem) {
        if (eq_Equation == eq_Equation_Table) {
            menuItem.setOnAction(e -> {
                boolean foundMatch = false;
                for (int i = 0; i < eq_TableView.getItems().size() && !foundMatch; i++) {
                    MenuItem targetMenuItem = (MenuItem) e.getSource();
                    Label targetItemLabel = (Label) targetMenuItem.getGraphic();
                    IEquationDataView equationData = eq_TableView.getItems().get(i);
                    if (equationData.toStringSearch().toLowerCase()
                            .contains(targetItemLabel.getText().toLowerCase())) {
                        eq_TableView.requestFocus();
                        eq_TableView.getSelectionModel().select(i);
                        eq_TableView.getFocusModel().focus(i);
                        eq_TableView.scrollTo(i);
                        foundMatch = true;
                    };
                }
                if (!foundMatch) {
                    System.out.println("No match was found!");
                }
            });
        } else {
            menuItem.setOnAction(e -> {
                MenuItem targetMenuItem = (MenuItem) e.getSource();
                Label targetItemLabel = (Label) targetMenuItem.getGraphic();
                if (!searchTreeItemMatch(eq_TreeView.getRoot(), targetItemLabel.getText().toLowerCase())) {
                    System.out.println("No match was found!");
                };
            });
        }
    }

    /**
     * Fonction récursive qui sélectionne un TreeItem dans le TreeView
     * @param actualNode
     * @param entree
     * @return
     */
    private boolean searchTreeItemMatch(TreeItem<IEquationDataView> actualNode, String entree) {
        boolean foundMatch = false;
        if (actualNode != null) {
            if (actualNode.getValue().toStringSearch().toLowerCase().contains(entree)) {
                eq_TableView.requestFocus();
                eq_TreeView.getSelectionModel().select(actualNode);
                foundMatch = true;
            }
            if (!foundMatch) {
                foundMatch = searchTreeItemMatch(actualNode.nextSibling(), entree);
                if (!actualNode.isLeaf()) {
                    foundMatch = searchTreeItemMatch(actualNode.getChildren().get(0), entree);
                }
            } else {
                actualNode.setExpanded(true);
            }
        }
        return foundMatch;
    }

    private String getEquationText() {
        return this.eq_Equation_Table.getText();
    }

    /**
     * Get the currently selected item in a ViewEquation format.
     *
     * @return
     */

    public IEquationDataView getCurrentSelectedItemData(){
        return selectedEquationData != null
                ? controller.createEquationData(this.selectedEquationData.getCategory(), this.selectedEquationData.getFunction())
                : null;
    }

    public void setGraphiqueController(GraphiqueView graphiqueController) {
        this.graphiqueController = graphiqueController;
    }

    /**
     * Building the Treeview
     */
    private List<String> getCategories(List<IEquationDataView> equations) {
        return equations.stream().map(IEquationDataView::getCategory).distinct().collect(Collectors.toList());
    }

    private Map<String, List<IEquationDataView>> getEquationsByCategories(List<IEquationDataView> equations) {
        Map<String, List<IEquationDataView>> map = new HashMap<>();
        List<String> categories = getCategories(equations);
        for (String cat : categories) {
            map.put(cat, equations.stream().filter(e -> e.getCategory().equals(cat)).collect(Collectors.toList()));
        }
        return map;
    }

    private void buildTreeView(List<IEquationDataView> equations) {
        eq_TreeView.setRoot(new TreeItem<>(new CategorieFolder("root")));
        List<String> categories = getCategories(equations);
        for (String cat : categories) {
            addFolderInTree(eq_TreeView.getRoot(), cat, 0);
        }
        addEquationToTree(equations);
    }

    private void addFolderInTree(TreeItem<IEquationDataView> treeItem, String categorie, int index) {
        String[] cats = categorie.split("/");
        if (index == cats.length) {
            treeItem.getChildren().add(new TreeItem<>(new CategorieFolder(categorie)));
            return;
        }
        if (cats.length == 1) {
            eq_TreeView.getRoot().getChildren().add(new TreeItem<>(new CategorieFolder(categorie)));
            return;
        } else {
            for (TreeItem<IEquationDataView> item : treeItem.getChildren()) {
                if (item.getValue().toString().equals(cats[index])) {
                    addFolderInTree(item, categorie, index + 1);
                    return;
                }
            }
            treeItem.getChildren().add(new TreeItem<>(new CategorieFolder(categorie)));
        }
    }

    private void addEquationToTree(List<IEquationDataView> equations){
        for (IEquationDataView equation: equations){
            addEquation(eq_TreeView.getRoot(),equation);
        }
    }

    private void addEquation(TreeItem<IEquationDataView> treeItem, IEquationDataView equation){
        if (treeItem.getValue().getCategory().equals(equation.getCategory())){
            treeItem.getChildren().add(new TreeItem<>(equation));
            return;
        }
        if (treeItem.getChildren().size()>0){
            addEquation(treeItem.getChildren().get(0),equation);
        }
        if (treeItem.nextSibling() != null){
            addEquation(treeItem.nextSibling(),equation);
        }

    }

    private void refreshTreeView(){
        new Thread(() -> {
                Platform.runLater(this::loadTreeView);
        }).start();
    }

    private void loadTreeView(){
        eq_TreeView.setRoot(null);
        eq_TreeView.setEditable(false);
        buildTreeView(controller.getDatabaseEquations());
        eq_TreeView.getRoot().setExpanded(true);
        eq_TreeView.setEditable(true);
    }

}







