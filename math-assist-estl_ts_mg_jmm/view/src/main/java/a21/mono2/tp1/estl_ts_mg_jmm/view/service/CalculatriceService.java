package a21.mono2.tp1.estl_ts_mg_jmm.view.service;

import a21.mono2.tp1.estl_ts_mg_jmm.view.inversion.ICalculatriceControllerView;
import javafx.concurrent.Service;
import javafx.concurrent.Task;

public class CalculatriceService extends Service<Double> {

    private String equation;

    private ICalculatriceControllerView controller;

    public CalculatriceService(String equation, ICalculatriceControllerView controller) {
        this.equation = equation;
        this.controller = controller;
    }

    @Override
    protected Task<Double> createTask() {
        return new Task<Double>() {
            @Override
            protected Double call() throws Exception {
                return controller.calculate(equation);
            }
        };
    }
}
