package a21.mono2.tp1.estl_ts_mg_jmm.view.inversion;

import a21.mono2.tp1.estl_ts_mg_jmm.view.exception.ViewInvalidEquationException;

public interface IGraphiqueControllerView {
    Double calculatePoint(String equation, Double xAxis) throws ViewInvalidEquationException;
}
