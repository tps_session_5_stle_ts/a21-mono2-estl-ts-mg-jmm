package a21.mono2.tp1.estl_ts_mg_jmm.view;

import a21.mono2.tp1.estl_ts_mg_jmm.view.exception.ViewInvalidEquationException;
import a21.mono2.tp1.estl_ts_mg_jmm.view.factory.ViewControllerFactory;
import a21.mono2.tp1.estl_ts_mg_jmm.view.inversion.IGraphiqueControllerView;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;

import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

/**
 * The View responsible for the graphique.fxml file.
 */
public class GraphiqueView implements Initializable {
    @FXML
    private LineChart<Number, Number> graph_Display;

    @FXML
    private BorderPane graph_BorderPane;

    @FXML
    private TextField graph_MinX, graph_MaxX, graph_MinY, graph_MaxY;

    @FXML
    private NumberAxis graph_Display_yAxis, graph_Display_xAxis;

    @FXML
    private GraphiquePointView graphiquePointController;

    private IGraphiqueControllerView controller;

    /**
     * The Series containing the current equation's points and data.
     */
    private XYChart.Series<Number, Number> equationSerie;

    /**
     * The drawing processus :
     * 0) : Nothing
     *
     * 1) : drawNewGraphique(Equation coming from EquationController)
     * 2) : drawGraphique (Equation coming from drawNewGraphique)
     * 3) : addPointInLineChart (For each xAxis points)
     * 4) : setGraphBounds
     *
     */


    /**
     * Draw the graph with basic points.
     *
     * @param equation The equation to draw
     */
    public void drawGraphique(String equation) {
        List<Double> xAxisPoints = List.of(0.0, 20.0, 40.0, 60.0, 80.0, 100.0);

        equationSerie = new XYChart.Series<Number, Number>();
        equationSerie.setName(equation);

        xAxisPoints.stream().forEach(xAxis -> {
            addPointInLineChart(xAxis, equationSerie.getName());
        });
        graph_Display.getData().add(equationSerie);

        setGraphBounds(xAxisPoints);
    }

    /**
     * Set the bounds of the graph (X and Y).
     *
     * @param xAxis
     */
    private void setGraphBounds(List<Double> xAxis) {
        graph_Display_xAxis.setLowerBound(xAxis.get(0).intValue() - 1);
        graph_Display_xAxis.setUpperBound(xAxis.get(xAxis.size() - 1).intValue() + 1);

        graph_Display_yAxis.setAutoRanging(false);
    }

    /**
     * Add point in the equation serie.
     *
     * @param xAxis
     * @param equation
     */
    private void addPointInLineChart(Double xAxis, String equation) {
        try {
            Double yAxis = controller.calculatePoint(equation, xAxis);
            equationSerie.getData().add(new XYChart.Data<Number, Number>(xAxis, yAxis));
        } catch (ViewInvalidEquationException e) {
            // Si la function ou l'équation finale n'est pas valide pour ce point.
            new ErrorManager().showError("Le point : " + xAxis.toString() + " de l'équation : " + equation + " porte problèmes.", 4000);
        }

    }

    /**
     * Add point in the equation serie.
     * Is called from the GraphiquePointController.
     *
     * @param xAxis
     */
    public void addPointInLineChart(Double xAxis) {
        addPointInLineChart(xAxis, equationSerie.getName());
    }

    /**
     * Remove a given point in the equation Serie.
     *
     * @param xAxisToRemove
     */
    public void removePointInLineChart(Double xAxisToRemove) {
        Iterator<XYChart.Data<Number, Number>> it = equationSerie.getData().iterator();
        while (it.hasNext()) {
            XYChart.Data<Number, Number> xAxis = it.next();

            // If the iterator's axis is the one to remove.
            if (xAxis.getXValue().doubleValue() == xAxisToRemove) {
                it.remove();
                break;
            }
        }
    }

    /**
     * Draw a new graph.
     * Is called when a new Equation is clicked (in EquationController).
     *
     * @param newEquation The equation to draw
     */
    public void drawNewGraphique(String newEquation) {
        graph_Display.getData().clear();
        graphiquePointController.clearListView();
        drawGraphique(newEquation);
    }

    /**
     * Clear the graph's data.
     */
    public void clearGraphique() {
        graph_Display.getData().clear();
        graphiquePointController.clearListView();
        equationSerie = null;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.controller = ViewControllerFactory.getFactoryInstance().createGraphiqueController();
        this.graphiquePointController.setGraphiqueController(this);

        // And listeners on bound fields.
        addListenersOnTextfield(graph_MinX, graph_Display_xAxis, graph_MaxX);
        addListenersOnTextfield(graph_MinY, graph_Display_yAxis, graph_MaxY);
    }

    /**
     * Add listeners to textfields.
     *
     * @param graph_min The minimum axis TextField
     * @param axis      The axis
     * @param graph_max The maximum axis TextField
     */
    private void addListenersOnTextfield(TextField graph_min, NumberAxis axis, TextField graph_max) {
        graph_min.textProperty().addListener((observableValue, s, t1) -> {
            if (t1 == null || t1.equals("")) {
                t1 = "0";
            }

            t1 = t1.replace(",", ".");
            try {
                double givenMin = Double.parseDouble(t1);
                axis.setLowerBound(givenMin);
            } catch (NumberFormatException nfe) {
                // If the given text isn't a number
                new ErrorManager().showError("Seulement des chiffres doivent être entrés ici.", 2000);
                // Set the TextField value before the letter was entered.
                graph_min.setText(s);
            }
        });

        graph_max.textProperty().addListener((observableValue, s, t1) -> {
            if (t1 == null || t1.equals("")) {
                t1 = "100";
            }

            t1 = t1.replace(",", ".");
            try {
                double givenMax = Double.parseDouble(t1);
                axis.setUpperBound(givenMax);
            } catch (NumberFormatException nfe) {
                // If the given text isn't a number
                new ErrorManager().showError("Seulement des chiffres doivent être entrés ici.", 2000);
                // Set the TextField value before the letter was entered.
                graph_max.setText(s);
            }
        });
    }

    public void setVisible() {
        this.graph_BorderPane.setVisible(!graph_BorderPane.isVisible());
    }
}
