package a21.mono2.tp1.estl_ts_mg_jmm.view.treeCellItems;


public class CategorieFolder extends Equation {



    public CategorieFolder(String parameter, String function, String expression) {
        super(parameter, function, expression);
    }


    public CategorieFolder(String cat) {
        super(cat);
    }

    @Override
    public String toString() {
        String[] cats = getCategory().split("/");
        return cats[cats.length-1];
    }
}
