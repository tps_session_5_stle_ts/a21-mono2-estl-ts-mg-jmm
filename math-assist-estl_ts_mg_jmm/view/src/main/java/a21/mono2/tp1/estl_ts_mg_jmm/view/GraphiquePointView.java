package a21.mono2.tp1.estl_ts_mg_jmm.view;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * The View responsible for the graphiquepoint.fxml file.
 */
public class GraphiquePointView implements Initializable {

    @FXML
    private Button point_Add, point_Erase;

    @FXML
    private ListView<String> point_ListView;

    @FXML
    private TextField point_Point;

    private GraphiqueView graphiqueController;


    @FXML
    private void onAjouter() {

        try {
            // Get the xAxis point to add.
            Double doubleXAxis = Double.parseDouble(point_Point.getText());
            // Add the point to the graph.
            graphiqueController.addPointInLineChart(doubleXAxis);
            // Add the axis to the listview.
            point_ListView.getItems().add(point_Point.getText());

            // Empty TextField text.
            point_Point.setText("");
            // request focus on the TextField (if not will focus on ListView)
            point_Point.requestFocus();

        } catch (NumberFormatException nfe) {
            // If there was a problem at the Double.parseDouble().
            new ErrorManager().showError("Le point entré doit être un chiffre valide.", 3000);
        } catch (NullPointerException npe) {
            // If there is no equation Serie in GraphiqueController.
            new ErrorManager().showError("Une équation doit être sélectionnée pour y ajouter un point.", 3000);
        }
    }

    @FXML
    private void onEffacer() {

        try {
            // Get the currently selected xAxis from the ListView.
            String axisToRemove = point_ListView.getSelectionModel().getSelectedItem();

            // Transform the currently selected xAxis to a Double.
            Double doubleXAxisToRemove = Double.parseDouble(axisToRemove);

            // Remove this point in the graph.
            graphiqueController.removePointInLineChart(doubleXAxisToRemove);

            // Unselect point to delete.
            point_ListView.getSelectionModel().clearSelection();
            point_ListView.getItems().remove(axisToRemove);

            // request focus on the TextField (if not will focus on ListView)
            point_Point.requestFocus();

        } catch (NumberFormatException nfe) {
            new ErrorManager().showError("Il y a eu un problème pendant la transformation du point sélectionné.", 2500);
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        point_ListView.getSelectionModel().selectedItemProperty().addListener((observableValue, s, t1) -> {
            // Disable the EFFACER button if the ListView's selection is null (and the opposite).
            this.point_Erase.setDisable(t1 == null);
        });

        point_Point.textProperty().addListener((observableValue, s, t1) -> {
            // Disable the AJOUTER button if the TextField's text is empty (and the opposite).
            this.point_Add.setDisable(t1.equals(""));
        });
    }

    public void setGraphiqueController(GraphiqueView graphiqueController) {
        this.graphiqueController = graphiqueController;
    }

    public void clearListView() {
        point_ListView.getItems().clear();
    }

}
