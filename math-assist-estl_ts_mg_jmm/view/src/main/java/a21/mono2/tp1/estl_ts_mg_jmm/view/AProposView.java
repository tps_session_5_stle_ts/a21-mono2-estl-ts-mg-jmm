package a21.mono2.tp1.estl_ts_mg_jmm.view;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class AProposView {

    private Stage stage;

    public void afficher() {

        if (stage == null) {
            try {
                stage = new Stage();
                stage.setResizable(false);
                stage.setScene(new Scene(loadFXML()));
                stage.setOnCloseRequest((w) -> stage.hide());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        stage.show();
    }

    private Parent loadFXML() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(AProposView.class.getClassLoader().getResource("fxml/aPropos.fxml"));
        return fxmlLoader.load();
    }

}
