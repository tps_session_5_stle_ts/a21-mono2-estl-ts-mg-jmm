package a21.mono2.tp1.estl_ts_mg_jmm.view;

import a21.mono2.tp1.estl_ts_mg_jmm.view.inversion.IEquationControllerView;
import a21.mono2.tp1.estl_ts_mg_jmm.view.inversion.IEquationDataView;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.KeyCode;
import javafx.util.converter.DefaultStringConverter;

public class CustomTableCell extends TextFieldTableCell<IEquationDataView, String> {

    private final IEquationControllerView controller;
    private final String value;

    public CustomTableCell(IEquationControllerView controller, String value) {
        this.setConverter(new DefaultStringConverter());
        this.controller = controller;
        this.value = value;
    }

    @Override
    public void commitEdit(String item) {
        super.commitEdit(item);
        controller.modifyDatabaseEquation((((EquationDecorator) getTableRow().getItem()).getInnerEquation()));
    }

    @Override
    public void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);
        if (empty) {
            setText(null);
            setGraphic(null);
        } else {
            setText(item);
            IEquationDataView equationData = getTableRow().getItem();
            if (equationData != null) {
                if (value.equals("category")) {
                    equationData.setCategory(item);
                } else if (value.equals("function")) {
                    equationData.setFunction(item);
                }
            }
        }
    }
}
