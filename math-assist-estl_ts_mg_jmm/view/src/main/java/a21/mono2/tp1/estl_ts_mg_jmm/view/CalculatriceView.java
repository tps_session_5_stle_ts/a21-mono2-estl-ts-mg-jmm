package a21.mono2.tp1.estl_ts_mg_jmm.view;

import a21.mono2.tp1.estl_ts_mg_jmm.view.factory.ViewControllerFactory;
import a21.mono2.tp1.estl_ts_mg_jmm.view.inversion.IConfigDataView;
import a21.mono2.tp1.estl_ts_mg_jmm.view.inversion.IEquationDataView;
import a21.mono2.tp1.estl_ts_mg_jmm.view.inversion.ICalculatriceControllerView;
import a21.mono2.tp1.estl_ts_mg_jmm.view.service.CalculatriceService;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;

import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

/**
 * The View responsible for the calculatrice.fxml file.
 */
public class CalculatriceView implements Initializable {

    @FXML
    private BorderPane calc_BorderPane;

    @FXML
    private TextArea calc_InputEquation;

    @FXML
    private GridPane middleContainer;

    @FXML
    private GridPane bottomContainer;

    @FXML
    private Button calc_btnAdd,calc_btnRemove,calc_btnModify;

    @FXML
    private ComboBox<IConfigDataView> calc_cbConfig;

    private EquationView equationController;

    private ICalculatriceControllerView controller;

    private List<Node> allButtons;
    private List<Button> editButtons;

    public void setEquationController(EquationView equationController) {
        this.equationController = equationController;
    }

    public void setVisible() {
        calc_BorderPane.setVisible(!calc_BorderPane.isVisible());
    }

    @FXML
    private void onCharacterButtonClick(ActionEvent event) {
        if (!calc_InputEquation.getText().contains("="))
            calc_InputEquation.setText(calc_InputEquation.getText() + ((Button) event.getSource()).getText());
    }

    @FXML
    private void onResetClick() {
        calc_InputEquation.setText("");
    }

    @FXML
    private void onEqualClick() {
        // Replace all "x" to "*" for mXparser.
        String equation = calc_InputEquation.getText().replaceAll("x", "*");

        // If the equation already contains a "=", so it is already calculated.
        if (equation.contains("=")) {
            return;
        }

        calculateEquation(equation);
    }

    /**
     * Calculate the equation with a service.
     *
     * @param equation
     */
    private void calculateEquation(String equation) {
        CalculatriceService service = new CalculatriceService(equation, controller);

        service.setOnSucceeded(e -> {
            calc_InputEquation.setText((equation + " = " + service.getValue()).replaceAll("\\*", "x"));
            activateUIElements();
        });

        service.setOnFailed(e -> {
            new ErrorManager().showError("Équation invalide", 1500);
            activateUIElements();
        });

        service.setOnRunning(e -> desactivateUIElements());

        service.start();
    }

    /**
     * Desactivate all UI buttons.
     */
    private void desactivateUIElements() {
        this.allButtons.stream().forEach(button -> button.setDisable(true));
    }

    /**
     * Activate all UI buttons.
     */
    private void activateUIElements() {
        this.allButtons.stream().forEach(button -> button.setDisable(false));
    }





    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.controller = ViewControllerFactory.getFactoryInstance().createCalculatriceController();

        // Get all buttons
        this.allButtons = middleContainer.getChildren().stream().flatMap(x -> ((GridPane) x).getChildren().stream()).collect(Collectors.toList());

        refreshConfigs();
        calc_btnAdd.setOnMouseClicked(mouseEvent -> {
            IEquationDataView selectedEquation = equationController.getCurrentSelectedItemData();
            if (selectedEquation != null){
                controller.addConfigToDatabase(selectedEquation);
                refreshConfigs();
            }
        });
        calc_btnModify.setOnMouseClicked(mouseEvent -> {

            IConfigDataView selectedConfig = calc_cbConfig.selectionModelProperty().get().getSelectedItem();
            if (selectedConfig != null){
                // Replace the equation's "x" (or other parameter) by the text in equation input.
                calc_InputEquation.setText(
                        selectedConfig.getEquation().getExpression().replaceAll(
                                selectedConfig.getEquation().getParameter(),
                                "(" + calc_InputEquation.getText() + ")"));
            }
        });
        calc_btnRemove.setOnMouseClicked(mouseEvent -> {
            IConfigDataView selectedConfig = calc_cbConfig.selectionModelProperty().get().getSelectedItem();
            if (selectedConfig != null){
                controller.deleteConfigFromDatabase(selectedConfig.getId());
                refreshConfigs();
            }
        });
    }

    private void refreshConfigs(){
        calc_cbConfig.getItems().clear();
        List<IConfigDataView> configs = controller.getDatabaseConfigs();
        for (IConfigDataView con:configs){
            calc_cbConfig.getItems().add(con);
        }
    }
}
