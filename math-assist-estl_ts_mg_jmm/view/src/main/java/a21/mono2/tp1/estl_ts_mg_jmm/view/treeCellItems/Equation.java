package a21.mono2.tp1.estl_ts_mg_jmm.view.treeCellItems;

import a21.mono2.tp1.estl_ts_mg_jmm.view.inversion.IEquationDataView;

public class Equation implements IEquationDataView {
    private int id;
    private String category;
    private String function;
    private String parameter;
    private String expression;

    public Equation() {
        this.id = 0;
        this.category = "None";
        this.function = "f(x) = x";
        this.parameter = "x";
        this.expression = "x";
    }

    public Equation(String category) {
        this.id = 0;
        this.category = category;
        this.function = "";
        this.parameter = "";
        this.expression = "";
    }

    public Equation(String function, String parameter, String expression) {
        this.id = 0;
        this.category = "";
        this.function = function;
        this.parameter = parameter;
        this.expression = expression;
    }

    public Equation(int id, String category, String function, String parameter, String expression) {
        this.id = id;
        this.category = category;
        this.function = function;
        this.parameter = parameter;
        this.expression = expression;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String getCategory() {
        return category;
    }

    @Override
    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public String getParameter() {
        return parameter;
    }

    @Override
    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    @Override
    public String getFunction() {
        return function;
    }

    @Override
    public void setFunction(String function) {
        this.function = function;
    }

    @Override
    public String getExpression() {
        return expression;
    }

    @Override
    public void setExpression(String expression) {
        this.expression = expression;
    }

    @Override
    public String toString() {
        return function;
    }

    @Override
    public String toStringSearch() {
        return id + category + function + parameter + expression;
    }
}