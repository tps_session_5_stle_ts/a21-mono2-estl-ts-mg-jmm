package a21.mono2.tp1.estl_ts_mg_jmm.view.utilTreeView;

import a21.mono2.tp1.estl_ts_mg_jmm.view.inversion.IEquationControllerView;
import a21.mono2.tp1.estl_ts_mg_jmm.view.inversion.IEquationDataView;
import a21.mono2.tp1.estl_ts_mg_jmm.view.treeCellItems.CategorieFolder;
import javafx.scene.control.TreeItem;

public class UtilTreeView {

    private IEquationControllerView controller;

    public UtilTreeView(IEquationControllerView controller){
        this.controller = controller;
    }






    public void updateCategoryInChildren(TreeItem<IEquationDataView> treeItem, String category, int index){
        if (treeItem == null){
            return;
        }

        if (!(treeItem.getValue() instanceof CategorieFolder)){
            String[] categ = treeItem.getValue().getCategory().split("/");
            categ[categ.length-index] = category;
            StringBuilder retCategory = new StringBuilder();
            retCategory.append(categ[0]);
            for (int i = 1; i < categ.length; i++) {
                retCategory.append("/").append(categ[i]);
            }
            treeItem.getValue().setCategory(retCategory.toString());
            controller.modifyDatabaseEquation(treeItem.getValue());
        }

        if (treeItem.nextSibling() != null){
            updateCategoryInChildren(treeItem.nextSibling(),category,index);
        }
        if (treeItem.getChildren().size()>0){
            updateCategoryInChildren(treeItem.getChildren().get(0),category,index+1);
        }
    }

    public void deleteChildren(TreeItem<IEquationDataView> treeItem){
        if (treeItem.getChildren().size()>0){
            deleteChildren(treeItem.getChildren().get(0));
        }
        if (treeItem.nextSibling() != null){
            deleteChildren(treeItem.nextSibling());
        }
        if (!(treeItem.getValue() instanceof CategorieFolder)){
            controller.deleteDatabaseEquation(treeItem.getValue().getId());
        }
        treeItem.getParent().getChildren().remove(treeItem);
    }

    // region ---- CALCUL DU NOMBRE D'ÉQUATION PAR CATÉGORIE ----

    public int nombreParCategorie(TreeItem<IEquationDataView> treeItem,String categorie){
        return calculEquationPerCategory(getItem(treeItem,categorie));
    }

    private TreeItem<IEquationDataView> getItem(TreeItem<IEquationDataView> treeItem, String categorie){
        TreeItem<IEquationDataView> retour = null;
        if (treeItem.getValue().getCategory().equals(categorie)){
            if (treeItem.getChildren().size()>0){
                return treeItem.getChildren().get(0);
            } else{
                return null;
            }

        } else{
            if (treeItem.getChildren().size()>0){
                retour = getItem(treeItem.getChildren().get(0),categorie);
            }
            if (treeItem.nextSibling() != null){
                retour = getItem(treeItem.nextSibling(),categorie);
            }
        }

        return retour;
    }
    private int calculEquationPerCategory(TreeItem<IEquationDataView> treeItem){
        int counter = 0;
        if (treeItem == null){
            return 0;
        }
        if (treeItem.isLeaf() && !(treeItem.getValue() instanceof CategorieFolder)){
            counter++;
        }
        if (treeItem.nextSibling() != null){
            counter += calculEquationPerCategory(treeItem.nextSibling());
        }
        if (treeItem.getChildren().size() > 0){
            counter += calculEquationPerCategory(treeItem.getChildren().get(0));
        }
        return counter;
    }

    //endregion
}
