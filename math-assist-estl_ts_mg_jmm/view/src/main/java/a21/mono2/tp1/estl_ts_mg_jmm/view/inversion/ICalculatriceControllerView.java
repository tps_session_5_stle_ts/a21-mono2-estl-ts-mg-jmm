package a21.mono2.tp1.estl_ts_mg_jmm.view.inversion;

import a21.mono2.tp1.estl_ts_mg_jmm.view.exception.ViewInvalidEquationException;

import java.util.List;

public interface ICalculatriceControllerView {

    public IConfigDataView createConfig(int id, IEquationDataView equation);
    void deleteConfig(int id);
    void addConfigToDatabase(IEquationDataView equation);
    void deleteConfigFromDatabase(int idConfig);
    void updateConfigFromDatabase(int id,IEquationDataView equation);
    List<IConfigDataView> getDatabaseConfigs();


    Double calculate(String equation) throws ViewInvalidEquationException;
}
