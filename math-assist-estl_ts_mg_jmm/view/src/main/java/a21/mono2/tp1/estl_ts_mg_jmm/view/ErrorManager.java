package a21.mono2.tp1.estl_ts_mg_jmm.view;

import a21.mono2.tp1.estl_ts_mg_jmm.view.service.TimePassingService;
import javafx.animation.FadeTransition;
import javafx.scene.control.Alert;

/**
 * The class responsible to create/display/manage an error.
 */
public class ErrorManager {

    /**
     * Show an alert dialog.
     *
     * @param message The message to display
     * @param timeMS  The time (in millis) to play the alert
     */
    public void showError(String message, int timeMS) {
        // Get Alert and FadeAnimation.
        Alert alert = getAlert(message);
        FadeTransition animation = getFadeAnimation(alert);

        // Create service.
        TimePassingService timeService = new TimePassingService(timeMS);

        // When it starts, open the alert.
        timeService.setOnScheduled(l -> alert.show());

        // When it finishes, start the fade out animation.
        timeService.setOnSucceeded(s -> animation.play());

        // Start the service.
        timeService.start();
    }

    /**
     * Get the alert's fade animation.
     *
     * @param alert
     * @return
     */
    private FadeTransition getFadeAnimation(Alert alert) {
        FadeTransition fadeAnimation = new FadeTransition();
        // Set from / to values.
        fadeAnimation.setFromValue(1.0);
        fadeAnimation.setToValue(0.0);

        // Apply the animation to the Alert body.
        fadeAnimation.setNode(alert.getDialogPane());

        // When the animation is finished close the Alert.
        fadeAnimation.setOnFinished(f -> alert.close());

        return fadeAnimation;
    }

    /**
     * Get an Alert.
     *
     * @param message
     * @return
     */
    private Alert getAlert(String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR);

        // Set body content and style.
        alert.setHeaderText(null);
        alert.setContentText(message);

        return alert;
    }
}
