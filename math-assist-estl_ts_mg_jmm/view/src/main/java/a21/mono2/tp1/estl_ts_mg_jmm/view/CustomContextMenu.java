package a21.mono2.tp1.estl_ts_mg_jmm.view;

import javafx.scene.Node;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Menu;
import javafx.scene.layout.Region;

public class CustomContextMenu extends ContextMenu {

    public CustomContextMenu(int maxHeight) {
        addEventHandler(Menu.ON_SHOWING, e -> {
            Node content = getSkin().getNode();
            if (content instanceof Region) {
                ((Region) content).setMaxHeight(getMaxHeight());
            }
        });
        this.setMaxHeight(maxHeight);
    }
}
