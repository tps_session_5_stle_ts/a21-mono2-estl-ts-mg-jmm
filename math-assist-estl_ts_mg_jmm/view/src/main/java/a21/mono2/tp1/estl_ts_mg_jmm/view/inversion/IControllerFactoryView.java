package a21.mono2.tp1.estl_ts_mg_jmm.view.inversion;

public interface IControllerFactoryView {
    IEquationControllerView createEquationController();

    IGraphiqueControllerView createGraphiqueController();

    ICalculatriceControllerView createCalculatriceController();
}
