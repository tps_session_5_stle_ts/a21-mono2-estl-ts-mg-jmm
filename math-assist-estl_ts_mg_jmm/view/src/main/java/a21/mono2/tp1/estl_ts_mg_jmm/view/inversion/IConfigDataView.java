package a21.mono2.tp1.estl_ts_mg_jmm.view.inversion;

public interface IConfigDataView {

    int getId();
    void setId(int id);

    IEquationDataView getEquation();
    void setEquation(IEquationDataView equation);

}
