package a21.mono2.tp1.estl_ts_mg_jmm.view;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.MenuItem;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * The main multicontroller.
 */
public class MainView implements Initializable {
    @FXML
    private EquationView equationController;

    @FXML
    private CalculatriceView calculatriceController;

    @FXML
    private GraphiqueView graphiqueController;

    @FXML
    private MenuItem main_MenuItem_Calculatrice;

    @FXML
    private MenuItem main_MenuItem_Graphique;

    @FXML
    private MenuItem main_MenuItem_APropos;

    private AProposView aProposController;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // Disable and hide graphique and its button
        graphiqueController.setVisible();
        main_MenuItem_Calculatrice.setDisable(true);
        // Put equationController in calculatriceController and graphiqueController
        calculatriceController.setEquationController(equationController);
        equationController.setGraphiqueController(graphiqueController);
        // Initialize aProposController
        aProposController = new AProposView();
    }

    @FXML
    void afficheAPropos() throws IOException {
        aProposController.afficher();
    }

    @FXML
    public void setVisible() {
        // Switch controllers visibility
        calculatriceController.setVisible();
        graphiqueController.setVisible();
        // Switch MenuItems disability
        main_MenuItem_Calculatrice.setDisable(!main_MenuItem_Calculatrice.isDisable());
        main_MenuItem_Graphique.setDisable(!main_MenuItem_Graphique.isDisable());
    }
}
