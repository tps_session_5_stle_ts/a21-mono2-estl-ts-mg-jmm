package a21.mono2.tp1.estl_ts_mg_jmm.view.service;

import javafx.concurrent.Service;
import javafx.concurrent.Task;

public class TimePassingService extends Service<Void> {
    private Integer timeMS;

    public TimePassingService(Integer timeMS) {
        this.timeMS = timeMS;
    }

    @Override
    protected Task<Void> createTask() {
        return new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                Thread.sleep(timeMS);
                return null;
            }
        };
    }
}
