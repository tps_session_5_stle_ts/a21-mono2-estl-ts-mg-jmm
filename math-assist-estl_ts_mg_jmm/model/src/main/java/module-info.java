module model {
    requires MathParser.org.mXparser;
    requires stdlib;

    exports a21.mono2.tp1.estl_ts_mg_jmm.model.exception;
    exports a21.mono2.tp1.estl_ts_mg_jmm.model.redBlackTree;
    exports a21.mono2.tp1.estl_ts_mg_jmm.model;
    exports a21.mono2.tp1.estl_ts_mg_jmm.model.inversion;
    exports a21.mono2.tp1.estl_ts_mg_jmm.model.factory;
    exports a21.mono2.tp1.estl_ts_mg_jmm.model.searchEngine;
}