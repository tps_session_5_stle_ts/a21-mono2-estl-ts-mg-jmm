package a21.mono2.tp1.estl_ts_mg_jmm.model.inversion;

public interface IEquationDataModel {
    int getId();
    String getCategory();
    String getParameter();
    String getFunction();
    String getExpression();
    void setId(int id);
    void setCategory(String category);
    void setParameter(String parameter);
    void setFunction(String function);
    void setExpression(String expresion);
    String toString();
    String toStringSearch();
}
