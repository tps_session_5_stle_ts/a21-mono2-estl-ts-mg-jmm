package a21.mono2.tp1.estl_ts_mg_jmm.model.factory;

import a21.mono2.tp1.estl_ts_mg_jmm.model.inversion.IControllerFactoryModel;

public class ModelControllerFactory {
    private static IControllerFactoryModel factory;

    public static IControllerFactoryModel getFactoryInstance() {
        return factory;
    }

    public static void setFactory(IControllerFactoryModel factory) {
        ModelControllerFactory.factory = factory;
    }
}
