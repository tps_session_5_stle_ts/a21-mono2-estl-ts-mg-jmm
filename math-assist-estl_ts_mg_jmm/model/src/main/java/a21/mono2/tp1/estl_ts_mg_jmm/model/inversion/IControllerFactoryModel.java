package a21.mono2.tp1.estl_ts_mg_jmm.model.inversion;

public interface IControllerFactoryModel {
    IEquationDataFactory createEquationDataFactory();
}
