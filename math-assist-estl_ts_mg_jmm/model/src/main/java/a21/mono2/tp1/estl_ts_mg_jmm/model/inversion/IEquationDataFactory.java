package a21.mono2.tp1.estl_ts_mg_jmm.model.inversion;

import java.util.List;

public interface IEquationDataFactory {
    IEquationDataModel createEquationData(String category, String function);

    IEquationDataModel createEquationData(int id, String category, String function);

    IEquationDataModel setFullEquationDataModel(IEquationDataModel equationData);

    List<IEquationDataModel> getDatabaseEquations();
}
