package a21.mono2.tp1.estl_ts_mg_jmm.model;

import a21.mono2.tp1.estl_ts_mg_jmm.model.exception.InvalidEquationException;
import org.mariuszgromada.math.mxparser.Expression;
import org.mariuszgromada.math.mxparser.Function;

public class Graphique {
    /**
     * Calculate a point's value in an equation.
     *
     * @param equation
     * @param xAxisPoint
     * @return
     * @throws InvalidEquationException
     */
    public Double calculatePoint(String equation, Double xAxisPoint) throws InvalidEquationException {
        Function function = new Function(equation);
        Expression e = new Expression("f(" + xAxisPoint + ")", function);

        // If the Function or Expression isn't valid.
        if (!function.checkSyntax() || !e.checkSyntax()) throw new InvalidEquationException();

        return e.calculate();
    }
}
