package a21.mono2.tp1.estl_ts_mg_jmm.model.searchEngine;

import a21.mono2.tp1.estl_ts_mg_jmm.model.Equation;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

public class CsvReader4000mots {
    public void csvReader(LinkedBlockingQueue<String> queue, String line, String splitBy) {
        try {
            //parsing a CSV file into BufferedReader class constructor
            String path =  String.valueOf(Equation.class.getResource("csv/4000mots.csv")
                    .getPath()).replaceFirst("/","");

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    new FileInputStream(path), StandardCharsets.UTF_8));

            while ((line = br.readLine()) != null) {
                String[] col = line.split(splitBy);
                queue.put(col[1]);
                System.out.println("4000mots: " + col[1]);
            }
        }
        catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
