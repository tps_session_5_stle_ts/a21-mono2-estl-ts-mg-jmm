package a21.mono2.tp1.estl_ts_mg_jmm.model.searchEngine;

import java.net.URL;
import java.util.concurrent.LinkedBlockingQueue;

public interface ICsvReader<T> {
    void csvReader(LinkedBlockingQueue<String> queue, String line, String splitBy);
}
