package a21.mono2.tp1.estl_ts_mg_jmm.model;

import org.mariuszgromada.math.mxparser.Function;

public class Equation {
    /**
     * Check if the entered equation is a valid Function.
     *
     * @param equation
     * @return
     */
    public Boolean isEquationValid(String equation) {
        Function function = new Function(equation);
        return function.checkSyntax() && function.getParametersNumber() == 1;
    }

    /**
     * Get a function's equation (the part after f(x) = ).
     *
     * @param function
     * @return
     */
    public String getEquationExpression(String function) {
        return new Function(function).getFunctionExpressionString();
    }

    /**
     * Get a function's parameter (the x in f(x) ...).
     *
     * @param function
     * @return
     */
    public String getEquationParameter(String function) {
        return new Function(function).getParameterName(0);
    }

}
