package a21.mono2.tp1.estl_ts_mg_jmm.model;

import a21.mono2.tp1.estl_ts_mg_jmm.model.exception.InvalidEquationException;
import org.mariuszgromada.math.mxparser.Expression;

public class Calculatrice {

    /**
     * Calculate an equation value.
     *
     * @param equation
     * @return
     * @throws InvalidEquationException
     */
    public Double calculate(String equation) throws InvalidEquationException {
        Expression expression = new Expression(equation);

        // If the expression isn't valid.
        if (!expression.checkSyntax()) {
            throw new InvalidEquationException();
        }

        return expression.calculate();
    }
}
