package a21.mono2.tp1.estl_ts_mg_jmm.model.searchEngine;

import a21.mono2.tp1.estl_ts_mg_jmm.model.factory.ModelControllerFactory;
import a21.mono2.tp1.estl_ts_mg_jmm.model.inversion.IEquationDataFactory;
import a21.mono2.tp1.estl_ts_mg_jmm.model.inversion.IEquationDataModel;

import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SearchEngine {

    private IEquationDataFactory factory;
    private ArrayList<String> values;
    private Map<String, List<String>> mappedValues;
    private LinkedBlockingQueue<String> filteredValues;

    private List<String> listFromDb;
    private Map<Integer, String> mapFromCsv4000mots;
    private List<String> listFromCsvMxParser;

    private Map<String, List<String>> mapFragmentsDeMots;

    /**
     * Constructeur vide.
     */
    public SearchEngine() {
        this.factory = ModelControllerFactory.getFactoryInstance().createEquationDataFactory();
        DataFetcher();
    }

    public LinkedBlockingQueue<String> getFilteredValues() {
        return filteredValues;
    }

    private void submitFetcherThreads(ExecutorService pool, LinkedBlockingQueue<String> queue) {
        pool.submit(() -> {
            for (IEquationDataModel equationData : factory.getDatabaseEquations()) {
                try {
                    queue.put(equationData.getFunction());
                    System.out.println("database: " + equationData.getFunction());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        pool.submit(() -> new CsvReader4000mots().csvReader(queue,"",";"));

        pool.submit(() -> new CsvReaderMxParser().csvReader(queue,"",";"));
    }

    /**
     * Méthode appelé pour initiliasier les listes. Peut aussi servir de refresh des listes.
     */
    public void DataFetcher() {
        // Fetch Data from Database
        ExecutorService pool = Executors.newFixedThreadPool(5);
        LinkedBlockingQueue<String> queueSimple = new LinkedBlockingQueue<>();
        LinkedBlockingQueue<String> queueComplexe = new LinkedBlockingQueue<>();
        values = new ArrayList<>();
        mappedValues = new HashMap<>();
        filteredValues = new LinkedBlockingQueue<>();

        // Va chercher les values pour queueSimple
        submitFetcherThreads(pool, queueSimple);

        // Va cherche les values pour queueComplexe
        submitFetcherThreads(pool, queueComplexe);

        // Prend les values de queueSimple et les met dans values
        pool.submit(() -> {
            while (queueSimple.size() > 0) {
                try {
                    String thisValue = queueSimple.take();
                    values.add(thisValue);
                    System.out.println("Added value: " + thisValue);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        // Prend les values de queueComplexe et les met dans mappedValues
        pool.submit(() -> {
            try {
                String mot = queueComplexe.take();
                String[] fragments = mot.split("");
                for (String fragment : fragments) {
                    mappedValues.get(fragment).add(mot);
                }
                System.out.println(mappedValues.get(fragments));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        try {
            pool.shutdown();
            pool.awaitTermination(60, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Méthode qui consiste à rechercher, parmis les listes,
     * un mot commençant par le fragment recherché.
     * @param fragmentRecherche
     * @return une liste de String commençant par fragmentRechercher.
     */
    public Runnable rechercheSimple(String fragment){
        return () -> {
            filteredValues = new LinkedBlockingQueue<>();
            System.out.println("New recherche");
            for (String value: values) {
                if (value.startsWith(fragment)) {
                    try {
                        filteredValues.put(value);
                        System.out.println("filteredvalue: " + value);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
    }

    /**
     * Sert à lancer la recherche, à partir dans la map, selon le fragment recherché.
     * @param fragmentRechercher
     * @return une liste de String contenant fragmentRechercher.
     */
    public Runnable rechercheComplexe(String fragmentRechercher){
        return () -> {
            for (String resultat: mappedValues.get(fragmentRechercher)) {
                try {
                    filteredValues.put(resultat);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
    }

    /**
     * Prépare la map [mapFragmentsDeMots].
     * Fait une seule grande liste de String qui contient les données BD et les fichiers,
     * puis place chaque mot de la liste dans la map.
     */
    public void prepareRechercheComplexe() {
        List<String> list = new ArrayList<>();
        list.addAll(listFromDb);
        list.addAll(mapFromCsv4000mots.values());
        list.addAll(listFromCsvMxParser);
        for (String s: list) {
           placerMotDansMap(s);
        }
    }

    /**
     * OVERLOAD SIMPLE
     * Méthode simple pour passer des paramètres non nécessaire.
     * @param mot
     */
    private void placerMotDansMap(String mot) {
        placerMotDansMap(mot.trim(), mot.trim());
    }

    /**
     * Méthode qui place le mot qu'on lui passe dans une map, en le décortiquant.
     * La clé correspond à chaque partie du mot décortiqué.
     * La valeur correspond à une liste de String, String qui contient biensûr la clé à l'intérieur.
     * Ex: AVION
     * Clé: a, av, avi, avio, avion, v, vi, vio, vion, etc...
     * Valeur: avion, avion, avion, avion, avion, avion, avion, avion, avion, etc...
     * @param mot
     * @param originalWord
     */
    //TODO: Améliorer la fonction avec la récursivité pure. (Pour l'instant, elle est semi-récursive).
    //TODO: Trouver une solution pour les accents qui s'affiche mal dans la console.
    private void placerMotDansMap(String mot, String originalWord) {
        StringBuilder sb = new StringBuilder();
        for (char c: mot.toCharArray()) {
            sb.append(c);
            if(mapFragmentsDeMots.containsKey(sb.toString())) {
                List<String> currentList = mapFragmentsDeMots.get(sb.toString());
                if(!currentList.contains(originalWord)) {
                    currentList.add(originalWord);
                }
                mapFragmentsDeMots.put(sb.toString(),currentList);
            } else {
                List<String> currentList = new ArrayList<>(List.of(String.valueOf(originalWord)));
                mapFragmentsDeMots.put(sb.toString(),currentList);
            }
        }
        if(mot.length() != 0 && mot.length()-1 <= originalWord.length()-1) {
                placerMotDansMap(mot.substring(1), originalWord);
        }
    }




    public static void main(String[] args) { // TODO: RETIRÉ CECI (TESTS)
        SearchEngine se = new SearchEngine();

        // BD
        //List<String> resultats = se.rechercheSimple("5");
        //resultats.forEach(System.out::println);

        // CSV
        //List<String> motsCsv = new ArrayList<>();
        //se.csvReader4000mots().forEach((integer, s) -> System.out.println(integer + " " + s)); // Lecture
        //se.csvReader4000mots().forEach((integer, s) -> motsCsv.add(s));

        //List<String> resultats = se.rechercheSimple("sin");
        //resultats.forEach(System.out::println);

        System.out.println(se.rechercheSimple("ma"));
        System.out.println(se.rechercheComplexe("s"));
        se.mapFragmentsDeMots.entrySet().forEach(System.out::println);
    }
}

