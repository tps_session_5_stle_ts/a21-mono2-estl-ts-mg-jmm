package a21.mono2.tp1.estl_ts_mg_jmm.model.exception;

public class InvalidEquationException extends Exception {
    public InvalidEquationException() {
    }

    public InvalidEquationException(String message) {
        super(message);
    }
}
