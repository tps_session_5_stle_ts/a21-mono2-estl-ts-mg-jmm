package a21.view.mono2.tp1.estl_ts_mg_jmm.database.connexionDB;

import a21.mono2.tp1.estl_ts_mg_jmm.data.ConfigData;
import a21.mono2.tp1.estl_ts_mg_jmm.data.EquationData;
import a21.view.mono2.tp1.estl_ts_mg_jmm.database.model.Config;
import a21.view.mono2.tp1.estl_ts_mg_jmm.database.model.Equation;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.Query;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DBConnexion {

    private static SessionFactory sessionFactory;

    public DBConnexion() {
        // If there is no sessionFactory (static), make one.
        if (sessionFactory == null) DBConnexion.prepareHibernate();
    }

    /**
     * On prepare la fabrique de session et on configure Hibernate
     */
    public static void prepareHibernate() {
        // Régler une seule fois pour toute la durée de l'application
        // configure les réglages dans le fichier hibernate.cfg.xml
        final StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
        try {
            sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        } catch (Exception e) {
            // The registry would be destroyed by the SessionFactory, but we had trouble
            // building the SessionFactory
            // so destroy it manually.
            e.printStackTrace();
            StandardServiceRegistryBuilder.destroy(registry);
        }
    }

    /**
     * On retrouve une session
     *
     * @return session
     */
    public static Session getSession() {
        if (sessionFactory == null) {
            DBConnexion.prepareHibernate();
        }
        return sessionFactory.openSession();
    }

    /**
     * Get the database Equations in a FullEquationData format.
     *
     * @return
     */
    public List<EquationData> getEquations() {
        Session session = DBConnexion.getSession();
        List<Equation> equations;
        equations = session.createQuery("from Equation").list();
        return equations.stream()
                .map(x -> new EquationData(x.getId(), x.getCategorie(), x.getFunction(), x.getParameter(), x.getExpression()))
                .collect(Collectors.toList());
    }

    /**
     * Add an Equation into the database.
     *
     * @param equationData
     */
    public void addEquation(EquationData equationData) {
        Session session = getSession();
        try {
            session.beginTransaction();

            Equation equation = new Equation(
                    equationData.getFunction(), equationData.getExpression(),
                    equationData.getParameter(), equationData.getCategory()
            );

            session.save(equation);

            // We will need to get the Equation's generated id
            // for the equationData object. (in the TableView)
            equationData.setId(equation.getId());
            session.getTransaction().commit();
            System.out.println("Addition effectuée");
        } catch (Exception e) {
            session.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    /**
     * Permet d'effacer une équation de la bd
     *
     * @param id le id de l'equation
     */
    public void deleteEquation(int id) {
        Session session = getSession();
        try {
            session.beginTransaction();
            Equation equation = session.get(Equation.class, id);
            session.delete(equation);
            session.getTransaction().commit();
            System.out.println("Deletion effectuée");
        } catch (Exception e) {
            session.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }


    /**
     * Update an Equation in the database.
     *
     * @param updatedData
     */
    public void modifyEquation(EquationData updatedData) {
        Session session = getSession();

        try {
            session.beginTransaction();
            Equation equation = session.get(Equation.class, updatedData.getId());

            // Set the fields.
            equation.setCategorie(updatedData.getCategory());
            equation.setFunction(updatedData.getFunction());
            equation.setCategorie(updatedData.getCategory());
            equation.setExpression(updatedData.getExpression());
            equation.setParameter(updatedData.getParameter());
            equation.setExpression(updatedData.getExpression());

            session.getTransaction().commit();
            System.out.println("Update effectuée");
        } catch (Exception e) {
            session.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }


    public void addConfig(EquationData equation) {
        Equation equa = new Equation(equation.getFunction(), equation.getExpression(), equation.getParameter(), equation.getCategory());
        Session session = getSession();
        try {
            session.beginTransaction();
            Config checkIfConfigExists = session.find(Config.class, equation.getId());

            // If the config doesn't exists in the database, ADD
            if (checkIfConfigExists == null) {
                Config config = new Config(equa);
                session.save(config);
                session.getTransaction().commit();
            }
        } catch (Exception e) {
            session.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    /**
     * Get all configs in a Map format.
     *
     * @return
     */
    public List<ConfigData> getConfigs() {
        Session session = DBConnexion.getSession();
        List<Config> configs = session.createQuery("from Config").list();

        return configs.stream()
                .map(l -> new ConfigData(l.getId(), new EquationData(l.getEquation().getId(),
                        l.getEquation().getCategorie(),l.getEquation().getFunction(),l.getEquation().getParameter(),
                        l.getEquation().getExpression())))
                .collect(Collectors.toList());
    }




    public void modifyConfig(int id, EquationData updatedData) {

        Session session = getSession();

        try {
            session.beginTransaction();
            Config config = session.get(Config.class, id);
            Equation equation = config.getEquation();

            // Set the fields.
            equation.setCategorie(updatedData.getCategory());
            equation.setFunction(updatedData.getFunction());
            equation.setCategorie(updatedData.getCategory());
            equation.setExpression(updatedData.getExpression());
            equation.setParameter(updatedData.getParameter());
            equation.setExpression(updatedData.getExpression());

            session.getTransaction().commit();
            System.out.println("Update effectuée");
        } catch (Exception e) {
            session.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public void deleteConfig(int idConfig) {
        Session session = getSession();
        try {
            session.beginTransaction();
            Config config = session.get(Config.class, idConfig);
            session.delete(config);
            session.getTransaction().commit();
            System.out.println("Deletion effectuée");
        } catch (Exception e) {
            session.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }
}
