package a21.view.mono2.tp1.estl_ts_mg_jmm.database.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "equation")
public class Equation {

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    @Column(name = "id")
    private int id;
    private String function;
    private String expression;
    private String parameter;
    private String categorie;
    private String chemin;


    public Equation() {
    }

    public Equation(String function, String expression, String parameter, String categorie) {
        this.function = function;
        this.expression = expression;
        this.parameter = parameter;
        this.categorie = categorie;
        this.chemin = "/";
    }

    public Equation(String function, String expression, String parameter, String categorie, String chemin){
        this(function,expression,parameter,categorie);
        this.chemin = chemin;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public String getParameter() {
        return parameter;
    }

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public String getChemin() {
        return chemin;
    }

    public void setChemin(String chemin) {
        this.chemin = chemin;
    }
}
