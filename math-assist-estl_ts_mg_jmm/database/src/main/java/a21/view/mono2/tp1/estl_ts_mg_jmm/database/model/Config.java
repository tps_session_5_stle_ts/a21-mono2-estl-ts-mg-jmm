package a21.view.mono2.tp1.estl_ts_mg_jmm.database.model;

import a21.mono2.tp1.estl_ts_mg_jmm.data.EquationData;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "config")
public class Config {

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    @Column(name = "config_id")
    private int id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id")
    private Equation equation;

    public Config() {
    }



    public Config(Equation equation) {
        this.equation = equation;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Equation getEquation() {
        return equation;
    }

    public void setEquation(Equation equation) {
        this.equation = equation;
    }
}
