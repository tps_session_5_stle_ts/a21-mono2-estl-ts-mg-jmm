module database {
    requires jdk.unsupported;
    requires java.persistence;
    requires org.hibernate.orm.core;
    requires java.sql;
    requires java.naming;
    requires data;

    exports com.springboot.sqlite;
    exports a21.view.mono2.tp1.estl_ts_mg_jmm.database.connexionDB;
    exports a21.view.mono2.tp1.estl_ts_mg_jmm.database.model;

    opens a21.view.mono2.tp1.estl_ts_mg_jmm.database.model to org.hibernate.orm.core;
}