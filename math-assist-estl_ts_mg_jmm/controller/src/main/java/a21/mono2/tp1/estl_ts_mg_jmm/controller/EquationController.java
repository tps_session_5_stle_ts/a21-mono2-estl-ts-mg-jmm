package a21.mono2.tp1.estl_ts_mg_jmm.controller;

import a21.mono2.tp1.estl_ts_mg_jmm.data.EquationData;
import a21.mono2.tp1.estl_ts_mg_jmm.model.Equation;
import a21.mono2.tp1.estl_ts_mg_jmm.model.searchEngine.SearchEngine;
import a21.mono2.tp1.estl_ts_mg_jmm.view.exception.ViewInvalidEquationException;
import a21.mono2.tp1.estl_ts_mg_jmm.view.inversion.IEquationDataView;
import a21.mono2.tp1.estl_ts_mg_jmm.view.inversion.IEquationControllerView;
import a21.view.mono2.tp1.estl_ts_mg_jmm.database.connexionDB.DBConnexion;

import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;

public class EquationController implements IEquationControllerView {

    private DBConnexion connexion = new DBConnexion();
    private Equation mathEquation = new Equation();
    private SearchEngine searchEngine = new SearchEngine();

    /**
     * Creates a default EquationData
     * @return
     */
    @Override
    public IEquationDataView createEquationData() {
        return new EquationData();
    }

    /**
     * Creates a new EquationData from a category and a function.
     * @param function
     * @return
     */
    @Override
    public IEquationDataView createEquationData(String category, String function) {
        return setFullEquationDataView(new EquationData(category, function));
    }

    /**
     * Creates a new EquationData from an id, a category and a function.
     * @param function
     * @return
     */
    @Override
    public IEquationDataView createEquationData(int id, String category, String function) {
        return setFullEquationDataView(new EquationData(id, category, function));
    }

    /**
     * Checks if a function is valid
     * @param function
     * @return
     */
    @Override
    public boolean isEquationValid(String function) {
        return mathEquation.isEquationValid(function);
    }

    /**
     * Sets parameter and expression of EquationData from its function
     * @param equationData
     */
    public IEquationDataView setFullEquationDataView(IEquationDataView equationData) {
        equationData.setParameter(mathEquation.getEquationParameter(equationData.getFunction()));
        equationData.setExpression(mathEquation.getEquationExpression(equationData.getFunction()));
        return equationData;
    }

    /**
     * Get all equations of the database.
     *
     * @return
     */
    @Override
    public List<IEquationDataView> getDatabaseEquations() {
        return connexion.getEquations().stream()
                .map(l -> createEquationData(l.getId(), l.getCategory(), l.getFunction()))
                .collect(Collectors.toList());
    }

    /**
     * Modify an existing equation into the database.
     * @param equationData
     */
    @Override
    public void modifyDatabaseEquation(IEquationDataView equationData) {
        connexion.modifyEquation((EquationData) setFullEquationDataView(equationData));
    }

    /**
     * Add an equation into the database.
     *
     * @param function
     * @throws ViewInvalidEquationException
     */
    @Override
    public IEquationDataView addDatabaseEquation(String category, String function) throws ViewInvalidEquationException {
        EquationData equationData;

        // If the equation is a valid Function.
        if (mathEquation.isEquationValid(function)) {
            // Get the equation's FullEquationData.
            equationData = (EquationData) createEquationData(category, function);
            // Add the new FullEquationData into the database.
            connexion.addEquation(equationData);
        } else throw new ViewInvalidEquationException();

        // Return the newly added FullEquationData
        return equationData;
    }

    /**
     * Delete an equation from the database from it's id.
     *
     * @param equationId
     */
    @Override
    public void deleteDatabaseEquation(int equationId) {
        connexion.deleteEquation(equationId);
    }

    @Override
    public Runnable getRechercheSimple(String entree) {
        return searchEngine.rechercheSimple(entree);
    }

    @Override
    public Runnable getRechercheComplexe(String entree) {
        return searchEngine.rechercheComplexe(entree);
    }

    @Override
    public LinkedBlockingQueue<String> getFilteredValues() {
        return searchEngine.getFilteredValues();
    }
}
