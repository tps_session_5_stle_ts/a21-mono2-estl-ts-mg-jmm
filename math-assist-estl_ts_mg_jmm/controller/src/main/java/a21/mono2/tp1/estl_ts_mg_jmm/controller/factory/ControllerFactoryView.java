package a21.mono2.tp1.estl_ts_mg_jmm.controller.factory;

import a21.mono2.tp1.estl_ts_mg_jmm.controller.CalculatriceController;
import a21.mono2.tp1.estl_ts_mg_jmm.controller.EquationController;
import a21.mono2.tp1.estl_ts_mg_jmm.controller.GraphiqueController;
import a21.mono2.tp1.estl_ts_mg_jmm.view.inversion.ICalculatriceControllerView;
import a21.mono2.tp1.estl_ts_mg_jmm.view.inversion.IControllerFactoryView;
import a21.mono2.tp1.estl_ts_mg_jmm.view.inversion.IEquationControllerView;
import a21.mono2.tp1.estl_ts_mg_jmm.view.inversion.IGraphiqueControllerView;

/**
 * The class responsible to create Controllers. (factory)
 */
public class ControllerFactoryView implements IControllerFactoryView {

    private static IControllerFactoryView factory;

    private ControllerFactoryView() {
    }

    /**
     * Get the application's ControllerFactory instance.
     *
     * @return
     */
    public static IControllerFactoryView getInstance() {
        if (factory == null) factory = new ControllerFactoryView();

        return factory;
    }

    /**
     * Create a Controller for EquationView.
     *
     * @return
     */
    @Override
    public IEquationControllerView createEquationController() {
        return new EquationController();
    }

    /**
     * Create a Controller for GraphiqueView.
     *
     * @return
     */
    @Override
    public IGraphiqueControllerView createGraphiqueController() {
        return new GraphiqueController();
    }

    /**
     * Create a Controller for CalculatriceView.
     *
     * @return
     */
    @Override
    public ICalculatriceControllerView createCalculatriceController() {
        return new CalculatriceController();
    }
}
