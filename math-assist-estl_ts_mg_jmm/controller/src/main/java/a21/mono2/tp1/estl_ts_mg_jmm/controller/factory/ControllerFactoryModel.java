package a21.mono2.tp1.estl_ts_mg_jmm.controller.factory;

import a21.mono2.tp1.estl_ts_mg_jmm.controller.EquationDataFactory;
import a21.mono2.tp1.estl_ts_mg_jmm.model.inversion.IControllerFactoryModel;
import a21.mono2.tp1.estl_ts_mg_jmm.model.inversion.IEquationDataFactory;

public class ControllerFactoryModel implements IControllerFactoryModel {
    private static IControllerFactoryModel factory;

    private ControllerFactoryModel() {
    }

    /**
     * Get the application's ControllerFactory instance.
     *
     * @return
     */
    public static IControllerFactoryModel getInstance() {
        if (factory == null) factory = new ControllerFactoryModel();

        return factory;
    }

    /**
     * Create a Controller for EquationView.
     *
     * @return
     */
    @Override
    public IEquationDataFactory createEquationDataFactory() {
        return new EquationDataFactory();
    }
}
