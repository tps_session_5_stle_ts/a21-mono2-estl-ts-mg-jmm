package a21.mono2.tp1.estl_ts_mg_jmm.controller;

import a21.mono2.tp1.estl_ts_mg_jmm.data.EquationData;
import a21.mono2.tp1.estl_ts_mg_jmm.model.inversion.IEquationDataFactory;
import a21.mono2.tp1.estl_ts_mg_jmm.model.inversion.IEquationDataModel;
import a21.mono2.tp1.estl_ts_mg_jmm.model.Equation;
import a21.view.mono2.tp1.estl_ts_mg_jmm.database.connexionDB.DBConnexion;

import java.util.List;
import java.util.stream.Collectors;

public class EquationDataFactory implements IEquationDataFactory {

    private Equation mathEquation = new Equation();
    private DBConnexion connexion = new DBConnexion();

    @Override
    public IEquationDataModel createEquationData(String category, String function) {
        return setFullEquationDataModel(new EquationData(category, function));
    }

    @Override
    public IEquationDataModel createEquationData(int id, String category, String function) {
        return setFullEquationDataModel(new EquationData(id, category, function));
    }

    @Override
    public IEquationDataModel setFullEquationDataModel(IEquationDataModel equationData) {
        equationData.setParameter(mathEquation.getEquationParameter(equationData.getFunction()));
        equationData.setExpression(mathEquation.getEquationExpression(equationData.getFunction()));
        return equationData;
    }

    @Override
    public List<IEquationDataModel> getDatabaseEquations() {
        return connexion.getEquations().stream()
                .map(l -> createEquationData(l.getId(), l.getCategory(), l.getFunction()))
                .collect(Collectors.toList());
    }
}
