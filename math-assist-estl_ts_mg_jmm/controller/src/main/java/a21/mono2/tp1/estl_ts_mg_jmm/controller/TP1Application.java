package a21.mono2.tp1.estl_ts_mg_jmm.controller;

import a21.mono2.tp1.estl_ts_mg_jmm.controller.factory.ControllerFactoryModel;
import a21.mono2.tp1.estl_ts_mg_jmm.controller.factory.ControllerFactoryView;
import a21.mono2.tp1.estl_ts_mg_jmm.model.factory.ModelControllerFactory;
import a21.mono2.tp1.estl_ts_mg_jmm.model.searchEngine.SearchEngine;
import a21.mono2.tp1.estl_ts_mg_jmm.view.TP1ApplicationLoader;
import a21.mono2.tp1.estl_ts_mg_jmm.view.factory.ViewControllerFactory;

public class TP1Application {

    public static void main(String[] args) {
        // We set the ViewControllerFactory as the ControllerFactory's instance
        ViewControllerFactory.setFactory(ControllerFactoryView.getInstance());
        ModelControllerFactory.setFactory(ControllerFactoryModel.getInstance());

        // Create a new TP1ApplicationLoader.
        TP1ApplicationLoader viewLoader = new TP1ApplicationLoader();
        // Load elements.
        viewLoader.load(args);
    }
}
