package a21.mono2.tp1.estl_ts_mg_jmm.controller;

import a21.mono2.tp1.estl_ts_mg_jmm.model.Graphique;
import a21.mono2.tp1.estl_ts_mg_jmm.model.exception.InvalidEquationException;
import a21.mono2.tp1.estl_ts_mg_jmm.view.exception.ViewInvalidEquationException;
import a21.mono2.tp1.estl_ts_mg_jmm.view.inversion.IGraphiqueControllerView;


public class GraphiqueController implements IGraphiqueControllerView {
    Graphique math = new Graphique();

    /**
     * Caclulate a point's (x) value in an equation.
     *
     * @param equation
     * @param xAxis
     * @return
     * @throws ViewInvalidEquationException
     */
    @Override
    public Double calculatePoint(String equation, Double xAxis) throws ViewInvalidEquationException {
        try {
            return math.calculatePoint(equation, xAxis);
        } catch (InvalidEquationException e) {
            throw new ViewInvalidEquationException();
        }

    }
}
