package a21.mono2.tp1.estl_ts_mg_jmm.controller;

import a21.mono2.tp1.estl_ts_mg_jmm.data.ConfigData;
import a21.mono2.tp1.estl_ts_mg_jmm.data.EquationData;
import a21.mono2.tp1.estl_ts_mg_jmm.view.exception.ViewInvalidEquationException;
import a21.mono2.tp1.estl_ts_mg_jmm.view.inversion.ICalculatriceControllerView;
import a21.mono2.tp1.estl_ts_mg_jmm.view.inversion.IConfigDataView;
import a21.view.mono2.tp1.estl_ts_mg_jmm.database.connexionDB.DBConnexion;
import a21.mono2.tp1.estl_ts_mg_jmm.model.Calculatrice;
import a21.mono2.tp1.estl_ts_mg_jmm.model.exception.InvalidEquationException;
import a21.mono2.tp1.estl_ts_mg_jmm.view.inversion.IEquationDataView;

import java.util.*;
import java.util.stream.Collectors;

public class CalculatriceController implements ICalculatriceControllerView {
    private DBConnexion connexion = new DBConnexion();
    private Calculatrice calculatrice = new Calculatrice();


    @Override
    public void deleteConfig(int id) {
        connexion.deleteConfig(id);
    }


    @Override
    public IConfigDataView createConfig(int id, IEquationDataView equation) {
        return new ConfigData(id,equation);
    }

    @Override
    public void addConfigToDatabase(IEquationDataView equation) {
        connexion.addConfig((EquationData) equation);
    }

    @Override
    public void deleteConfigFromDatabase(int idConfig) {
        connexion.deleteConfig(idConfig);
    }

    @Override
    public void updateConfigFromDatabase(int id,IEquationDataView config) {
        connexion.modifyConfig(id,(EquationData) config);
    }

    /**
     * Get a Map of all the Configs in the database.
     * @return
     */
    @Override
    public List<IConfigDataView> getDatabaseConfigs() {

        return connexion.getConfigs().stream()
                .map(l -> new ConfigData(l.getId(), l.getEquation()))
                .collect(Collectors.toList());
    }

    /**
     * Calculate an equation.
     * @param equation
     * @return
     * @throws ViewInvalidEquationException
     */
    @Override
    public Double calculate(String equation) throws ViewInvalidEquationException {
        try{
            return calculatrice.calculate(equation);
        } catch (InvalidEquationException e) {
            throw new ViewInvalidEquationException();
        }
    }
}