module controller {
    requires view;
    requires model;
    requires database;
    requires data;
}