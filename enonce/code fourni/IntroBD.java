package a21.climoilou.mono2.tps.tp1.model;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import java.util.List;


// TRES IMPORTANT la classe com.springboot.sqllite.SQLDialect doit être accessible
public class IntroBD {

    private static SessionFactory sessionFactory;

    /**
     * On prepare la fabrique de session et on configure Hibernate
     */
    public static void prepareHibernate() {
        // Régler une seule fois pour toute la durée de l'application
        // configure les réglages dans le ficheir hibernate.cfg.xml
        final StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
        try {
            sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        } catch (Exception e) {
            // The registry would be destroyed by the SessionFactory, but we had trouble
            // building the SessionFactory
            // so destroy it manually.
            e.printStackTrace();
            StandardServiceRegistryBuilder.destroy(registry);
        }
    }

    /**
     * On retrouve une session
     *
     * @return session
     */
    public static Session getSession() {
        if (sessionFactory == null) {
            IntroBD.prepareHibernate();
        }
        return sessionFactory.openSession();
    }

    public void addNombre(int value) {
        Session session = IntroBD.getSession();
        session.beginTransaction();
        session.save(new SimpleNombre(value));
        session.getTransaction().commit();
    }

    public List<SimpleNombre> getNombres() {
        Session session = IntroBD.getSession();
        List<SimpleNombre> nombres;
        nombres = session.createQuery("from SimpleNombre").list();
        return nombres;
    }


    public static void main(String[] args) {
        // Il faut obtenir une session pour travailler
        Session session = IntroBD.getSession();

        // Débutez une transaction
        session.beginTransaction();

        // Enregistrez trois nombres différents
        session.save(new SimpleNombre(32));
        session.save(new SimpleNombre(33));
        session.getTransaction().commit();


        // Récupérez la liste de nmobre avec la query "from SimpleNombre"
        List<SimpleNombre> nombres;
        nombres = session.createQuery("from SimpleNombre").list();

        // Affichez le tout
        System.out.println("la taille est " + nombres.size());
        System.out.println(nombres);

        session.close();

    }
}
